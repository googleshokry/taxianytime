<?php

//session_start();
//require_once  'database.php';
//error_reporting(false);

class ClientApns {

    public $ctx;
    public $fp;
    private $ssl = 'ssl://gateway.push.apple.com:2195';
    private $passphrase = '123456';
    private $sandboxCertificate = 'iph_cert/Client_certy.pem';
    private $sandboxSsl = 'ssl://gateway.sandbox.push.apple.com:2195';
    private $sandboxFeedback = 'ssl://feedback.sandbox.push.apple.com:2196';
    private $message = 'ManagerMaster';

    public function __construct() {
        $this->initialize_apns();
    }

    private function getCertificatePath() {
        /* return app_path() . '/ios_push/' . $this->sandboxCertificate; */
        return public_path() . '/apps/ios_push/' . $this->sandboxCertificate;
    }

    public function initialize_apns() {
        try {
            $this->ctx = stream_context_create();

            //stream_context_set_option($ctx, 'ssl', 'cafile', 'entrust_2048_ca.cer');
            stream_context_set_option($this->ctx, 'ssl', 'local_cert', $this->getCertificatePath());
            stream_context_set_option($this->ctx, 'ssl', 'passphrase', $this->passphrase); // use this if you are using a passphrase
            // Open a connection to the APNS servers
            $this->fp = @stream_socket_client($this->ssl, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $this->ctx);

            if ($this->fp) {
                //Log::info('Successfully connected to server of APNS');
                //echo 'Successfully connected to server of APNS ckUberForXOwner.pem';
            } else {
                //Log::error('Error in connection while trying to connect to APNS');
                //echo 'Error in connection while trying to connect to APNS ckUberForXOwner.pem';
            }
        } catch (Exception $e) {
            //Log::error($e);
        }
    }

    public function send_notification($devices, $message) {
        try {
            $errCounter = 0;
            $payload = json_encode(array('aps' => $message));
            $result = 0;
            $bodyError = '';
            foreach ($devices as $key => $value) {
                $msg = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', $value)) . pack('n', (strlen($payload))) . $payload;
                $result = fwrite($this->fp, $msg);
                $bodyError .= 'result: ' . $result . ', devicetoken: ' . $value;
                if (!$result) {
                    $errCounter = $errCounter + 1;
                }
            }
            //echo 'Result :- '.$result;
            if ($result) {
                //Log::info('Delivered Message to APNS' . PHP_EOL);
                //echo 'Delivered Message to APNS' . PHP_EOL;
                $bool_result = $this->checkAppleErrorResponse();
                $bool_result = true;
            } else {
                //Log::info('Could not Deliver Message to APNS' . PHP_EOL);
                //echo 'Could not Deliver Message to APNS' . PHP_EOL;
                $bool_result = false;
            }

            @socket_close($this->fp);
            @fclose($this->fp);
            return $bool_result;
        } catch (Exception $e) {
            //Log::error($e);
        }
    }
    public function checkAppleErrorResponse() {
        $result = false;
        //byte1=always 8, byte2=StatusCode, bytes3,4,5,6=identifier(rowID).
        // Should return nothing if OK.

        //NOTE: Make sure you set stream_set_blocking($this->fp, 0) or else fread will pause your script and wait
        // forever when there is no response to be sent.

        $apple_error_response = fread($this->fp, 6);

        if ($apple_error_response) {

            // unpack the error response (first byte 'command" should always be 8)
            $error_response = unpack('Ccommand/Cstatus_code/Nidentifier', $apple_error_response);

            if ($error_response['status_code'] == '0') {
                $error_response['status_code'] = '0-No errors encountered';

            } else if ($error_response['status_code'] == '1') {
                $error_response['status_code'] = '1-Processing error';

            } else if ($error_response['status_code'] == '2') {
                $error_response['status_code'] = '2-Missing device token';

            } else if ($error_response['status_code'] == '3') {
                $error_response['status_code'] = '3-Missing topic';

            } else if ($error_response['status_code'] == '4') {
                $error_response['status_code'] = '4-Missing payload';

            } else if ($error_response['status_code'] == '5') {
                $error_response['status_code'] = '5-Invalid token size';

            } else if ($error_response['status_code'] == '6') {
                $error_response['status_code'] = '6-Invalid topic size';

            } else if ($error_response['status_code'] == '7') {
                $error_response['status_code'] = '7-Invalid payload size';

            } else if ($error_response['status_code'] == '8') {
                $error_response['status_code'] = '8-Invalid token';

            } else if ($error_response['status_code'] == '10') {
                $error_response['status_code'] = '10-Shutdown';

            } else if ($error_response['status_code'] == '255') {
                $error_response['status_code'] = '255-None (unknown)';

            } else {
                $error_response['status_code'] = $error_response['status_code'].'-Not listed';

            }

            Log::info( 'ERROR:: Response Command:' . $error_response['command'] . '; Identifier:' . $error_response['identifier'] . '; Status:' . $error_response['status_code'] . '.' );
            Log::info( 'Identifier is the rowID (index) in the database that caused the problem, and Apple will disconnect you from server. To continue sending Push Notifications, just start at the next rowID after this Identifier.' );

        }else{
            Log::info( 'No ERRORS occurred in sending Push Notification.');
            $result = true;
        }
        return $result;
    }
}
